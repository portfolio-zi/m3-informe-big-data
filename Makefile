ENGINE = lualatex
FILENAME = base.tex		
CODE = $(shell grep -o -P '(?<=code\{).*(?=\})' $(FILENAME))
POSTNAME =
#POSTNAME = -ZIzaola

ifeq ($(CODE), )
	JOBNAME = base
else
	JOBNAME = $(CODE)
endif
FINALNAME = $(JOBNAME)$(POSTNAME)

all:
	make compile

compile: $(FILENAME) data-analysis.tex base.bib
	$(ENGINE) -shell-escape -jobname $(FINALNAME) $(FILENAME)
	biber $(JOBNAME)
	$(ENGINE) -shell-escape -jobname $(FINALNAME) $(FILENAME)
	$(ENGINE) -shell-escape -jobname $(FINALNAME) $(FILENAME)


# To reduce the filesize by reducing the resolution of included bitmaps.
# http://tex.stackexchange.com/questions/14429/pdftex-reduce-pdf-size-reduce-image-quality
compressimage:
	gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/prepress -dNOPAUSE -dQUIET -dBATCH -sOutputFile=$(FINALNAME)-reduced.pdf $(FINALNAME).pdf
	mv $(FINALNAME)-reduced.pdf   $(FINALNAME).pdf


data-analysis.tex: data-analysis.gnumeric pretify-table.awk
	ssconvert  data-analysis.gnumeric data-analysis.tex
	iconv -f iso-8859-1 -t utf8 data-analysis.tex -o data-analysis.tex
	./pretify-table.awk data-analysis.tex > tmp.tex
	mv tmp.tex data-analysis.tex


clean:
	rm $(JOBNAME).pdf *.aux *.log *.toc *.out *.bbl *.blg *.bcf *.run.xml *.hst *.ver data-analysis.tex *.sta

